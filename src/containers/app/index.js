import React from 'react'
import { Route } from 'react-router-dom'
import Tapper from '../tapper'

const App = () => (
  <div>
    <main>
      <Route exact basename="/beertaps" path="/beertaps" component={Tapper} />
    </main>
  </div>
)

export default App
