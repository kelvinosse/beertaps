import { getBeerOrder } from '../utils'

export const ON_INCREMENT = 'tapper/ON_INCREMENT'
export const INCREMENT = 'tapper/INCREMENT'
export const ON_DECREMENT = 'tapper/ON_DECREMENT'
export const DECREMENT = 'tapper/DECREMENT'
export const ORDER_BEERTAP = 'tapper/ORDER_BEERTAP'
export const CHANGE_BEERTAP_VALUE = 'tapper/CHANGE_BEERTAP_VALUE'
export const CALC_BEER_ORDER = 'tapper/CALC_BEER_ORDER'
export const RESTART = 'tapper/RESTART'

const initialState = {
  beerTap: '',
  beerOrder: {
    divisors: [],
    sumIsGreater: false,
    subsetNotEqual: false,
    perfect: false
  },
  calcInit: false,
  isIncrementing: false,
  isDecrementing: false,
  sumIsGreater: false,
  sumIsNotEqual: false,
  beerOrdered: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ON_INCREMENT:
      return {
        ...state,
        beerTap: parseInt(state.beerTap),
        isIncrementing: true
      }
    case INCREMENT:
      return {
        ...state,
        beerTap:
          state.beerTap >= 1 && state.beerTap < 1000 ? state.beerTap + 1 : 1,
        isIncrementing: !state.isIncrementing
      }
    case ON_DECREMENT:
      return {
        ...state,
        beerTap: parseInt(state.beerTap),
        isDecrementing: true
      }
    case DECREMENT:
      return {
        ...state,
        beerTap:
          state.beerTap > 1 && state.beerTap <= 1000 ? state.beerTap - 1 : 1000,
        isDecrementing: !state.isDecrementing
      }
    case ORDER_BEERTAP:
      return {
        ...state,
        beerOrdered: true
      }
    case CHANGE_BEERTAP_VALUE:
      return {
        ...state,
        beerTap: action.value
      }
    case CALC_BEER_ORDER:
      return {
        ...state,
        calcInit: true,
        beerOrder: getBeerOrder(parseInt(state.beerTap))
      }
    case RESTART:
      return {
        ...initialState
      }

    default:
      return state
  }
}

export const increment = () => {
  return dispatch => {
    dispatch({
      type: ON_INCREMENT
    })

    dispatch({
      type: INCREMENT
    })

    dispatch({
      type: CALC_BEER_ORDER
    })
  }
}

export const decrement = () => {
  return dispatch => {
    dispatch({
      type: ON_DECREMENT
    })

    dispatch({
      type: DECREMENT
    })

    dispatch({
      type: CALC_BEER_ORDER
    })
  }
}

export const orderBeer = () => {
  return dispatch => {
    dispatch({
      type: ORDER_BEERTAP
    })
  }
}

export const changeBeerTapValue = value => {
  return dispatch => {
    dispatch({
      type: CHANGE_BEERTAP_VALUE,
      value
    })

    if (value === '') {
      dispatch({
        type: RESTART
      })
    } else {
      dispatch({
        type: CALC_BEER_ORDER
      })
    }
  }
}

export const calcBeerOrder = () => {
  return dispatch => {
    dispatch({
      type: CALC_BEER_ORDER
    })
  }
}

export const restart = () => {
  return dispatch => {
    dispatch({
      type: RESTART
    })
  }
}
