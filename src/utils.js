const subSetNotEqual = (number, divisors) => {
  let subset = [[]]
  for (let i = 0; i < divisors.length; i++) {
    for (let k = 0, length = subset.length; k < length; k++) {
      let vals = subset[k].concat(divisors[i])
      subset.push(vals)
      let sum = vals.reduce((a, b) => a + b)
      if (sum === number) {
        return false
      }
    }
  }

  return true
}

export const getBeerOrder = number => {
  let divisors = []
  for (let i = 1; i <= number / 2; i++) {
    if (number % i === 0) {
      divisors = [...divisors, i]
    }
  }

  const sumIsGreater = divisors.reduce((a, b) => a + b, 0) > number
  const subsetNotEqual = subSetNotEqual(number, divisors)
  return {
    divisors,
    sumIsGreater,
    subsetNotEqual,
    perfect: sumIsGreater && subsetNotEqual
  }
}
